font-manager (0.8.8-2) unstable; urgency=medium

  * Team upload

  [ Jesús Soto ]
  * Remove nautilus extension since it's not compatible with nautilus
    43 (which uses GTK4 now)

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 16 Aug 2022 17:17:22 -0400

font-manager (0.8.8-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches: Drop all backported patches.
  * debian/copyright: Refreshed.

 -- Boyuan Yang <byang@debian.org>  Mon, 14 Feb 2022 12:49:47 -0500

font-manager (0.8.7-2) unstable; urgency=high

  * Add patch to fix FTBFS with Vala 0.54.1. (Closes: #997226)
  * Bump Standards-Version to 4.6.0.

 -- Boyuan Yang <byang@debian.org>  Mon, 25 Oct 2021 12:16:34 -0400

font-manager (0.8.7-1) unstable; urgency=medium

  * New upstream release.

 -- Boyuan Yang <byang@debian.org>  Mon, 16 Aug 2021 10:21:10 -0400

font-manager (0.8.6-1~exp1) experimental; urgency=medium

  * New upstream release.
  * debian/copyright: Update copyright information.

 -- Boyuan Yang <byang@debian.org>  Sat, 10 Apr 2021 23:40:51 -0400

font-manager (0.8.4-1) unstable; urgency=medium

  * New upstream release. 
  * Bump meson version requirement to 0.53+.

 -- Boyuan Yang <byang@debian.org>  Fri, 05 Feb 2021 15:19:19 -0500

font-manager (0.8.3-1) unstable; urgency=medium

  * New upstream release.

 -- Boyuan Yang <byang@debian.org>  Mon, 07 Dec 2020 22:02:31 -0500

font-manager (0.7.9-2) unstable; urgency=medium

  * No-change source-only upload.

 -- Boyuan Yang <byang@debian.org>  Wed, 07 Oct 2020 17:07:58 -0400

font-manager (0.7.9-1) unstable; urgency=medium

  * New upstream release.
  * Add new thunar-font-manager package for thunar file manager
    integration.
  * debian/patches: Drop reproducible patch, merged upstream by
    introducing a build-time option.
  * debian/rules: Enable build-time reproducible option.

 -- Boyuan Yang <byang@debian.org>  Thu, 01 Oct 2020 11:33:43 -0400

font-manager (0.7.8-2) unstable; urgency=medium

  * debian/control: Fix package description for nemo-font-manager.
    (Closes: #966341)

 -- Boyuan Yang <byang@debian.org>  Wed, 29 Jul 2020 14:31:33 -0400

font-manager (0.7.8-1) unstable; urgency=medium

  * New upstream release.

 -- Boyuan Yang <byang@debian.org>  Sun, 12 Jul 2020 21:30:51 -0400

font-manager (0.7.7-1) unstable; urgency=medium

  [ Debian Janitor ]
  * debian/changelog: Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove unused license definitions for LGPL-3+.

  [ Boyuan Yang ]
  * Take over package maintenance via ITS process. (Closes: #955637)
  * debian/control: Update package maintainer and uploaders.

 -- Boyuan Yang <byang@debian.org>  Wed, 22 Apr 2020 00:26:10 -0400

font-manager (0.7.7-0.3) unstable; urgency=medium

  * Team upload.
  * debian/patches/0001: Add patch to make the build reproducible.
    (Closes: #955009)

 -- Boyuan Yang <byang@debian.org>  Fri, 03 Apr 2020 12:19:40 -0400

font-manager (0.7.7-0.2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Boyuan Yang <byang@debian.org>  Fri, 03 Apr 2020 11:32:32 -0400

font-manager (0.7.7-0.2~exp2) experimental; urgency=high

  * debian/control: Add "Breaks: " relationship against older
    font-manager packages. This fixes file conflict during
    upgrade.

 -- Boyuan Yang <byang@debian.org>  Fri, 27 Mar 2020 13:34:07 -0400

font-manager (0.7.7-0.2~exp1) experimental; urgency=medium

  * Non-maintainer upload.
  * debian/: Merge with upstream packaging instructions:
    + Split package and provide font-manager and font-viewer
      in different packages.
    + Provide nautilus and nemo integration packages.
      (Closes: #955131)
  * debian/copyright: Update copyright information accordingly.

 -- Boyuan Yang <byang@debian.org>  Fri, 27 Mar 2020 13:04:48 -0400

font-manager (0.7.7-0.1) unstable; urgency=medium

  * Non-maintainer upload.
  * New upstream version 0.7.7.
    + Fix build error. (Closes: #949144)
  * debian/control:
    + Bump Standards-Version to 4.5.0.
    + Bump debhelper compat to v12.
    + Migrate package to python3. (Closes: #890151, #936539)
    + Update Vcs-* fields and put Debian git packaging repo under
      Salsa fonts-team group.
    + Add Debian Fonts team into the uploaders list for team
      maintenance.
  * debian/rules: Clean up old build instructions.
  * debian/copyright: Update information.
  * debian/compat: Dropped, no longer necessary.
  * debian/source/options, debian/source/local-options: Dropped,
    useless now.
  * debian/patches: Drop all patches, merged upstream.

 -- Boyuan Yang <byang@debian.org>  Mon, 16 Mar 2020 23:46:50 -0400

font-manager (0.7.3-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add add-ref-keyword.patch, fix-incompatible-types.patch:
    - Backport git patches to fix build with vala 0.36 (Closes: #873872)
  * Update Vcs fields

 -- Jeremy Bicha <jbicha@debian.org>  Wed, 20 Sep 2017 16:05:07 -0400

font-manager (0.7.3-1) unstable; urgency=medium

  * Imported Upstream version 0.7.3:
    - Fix Json.Serializable.find_property incompatible
      return type error. Bump valac requirement to 0.34.1.
      See https://github.com/GNOME/vala/commit/28fa5311
      for a rationale. Closes: #839350
  * Clean up patches, adopted upstream

 -- Alessio Treglia <alessio@debian.org>  Mon, 17 Oct 2016 00:16:14 +0100

font-manager (0.7.2+0git410aa802-3) unstable; urgency=medium

  * Get rid of manually created post{inst,rm} (Closes: #837084)

 -- Alessio Treglia <alessio@debian.org>  Thu, 08 Sep 2016 21:39:43 +0100

font-manager (0.7.2+0git410aa802-2) unstable; urgency=medium

  * Patch forwarded
  * Disable tests.
    They need some work to be fully functional.

 -- Alessio Treglia <alessio@debian.org>  Tue, 06 Sep 2016 14:38:00 +0100

font-manager (0.7.2+0git410aa802-1) unstable; urgency=medium

  * Imported Upstream version 0.7.2+0git410aa802
  * Remove manpages provided by Debian
  * Get rid of unnecessary build-deps
  * Turn file-roller presence check into into a conditional flag
  * Enable file-roller support
  * Fix Vito's last name
  * Remove Andrea Colangelo from Uploaders
  * Update the package's description
  * Refresh debian/rules and enable parallel builds
  * Refresh debian/copyright

 -- Alessio Treglia <alessio@debian.org>  Sat, 03 Sep 2016 01:17:02 +0100

font-manager (0.7.2-1) unstable; urgency=medium

  * Imported Upstream version 0.7.2 (Closes: #796817)
  * Update homepage (Closes: #830858)
  * Update debian/watch
  * debian/gbp.conf: set compression to gz
  * Update debian/copyright
  * Update patchset, add patch to avoid checking for
    the presence of file-roller at build time
  * Refresh build-dependencies
  * Refresh debian/rules
  * Refresh debian/docs
  * Remove debian/menu control file
  * Get rid of obsolete links file
  * Enable nautilus extension
  * Update Depends and Suggests fields
  * Fix Vcs fields
  * Add Vito Mulo to Uploaders field
  * Bump debhelper compat
  * Bump Standards

 -- Alessio Treglia <alessio@debian.org>  Tue, 30 Aug 2016 22:11:42 +0100

font-manager (0.5.7-4) unstable; urgency=low

  * Fix missing #includes to prevent FTBFS with GCC 4.7. (Closes: #672043)
  * debian/copyright:
    - Update copyright years.
    - Update to machine-readable format.
  * Bump Standards.

 -- Alessio Treglia <alessio@debian.org>  Wed, 09 May 2012 10:35:16 -0700

font-manager (0.5.7-3) unstable; urgency=low

  * debian/rules:
    - Enable dh-autoreconf add-on.
    - Pass --enable-debuginfo to configure to not strip objects
      from the library by default.
  * debian/control:
    - Bump debhelper build-dependency, override_* rules need
      dh >= 7.0.50~.
    - Build-depend on python-all-dev (>= 2.6.6-3).

 -- Alessio Treglia <alessio@debian.org>  Mon, 14 Mar 2011 12:35:50 +0100

font-manager (0.5.7-2) unstable; urgency=low

  * Don't remove cache dir if non-existent.

 -- Alessio Treglia <alessio@debian.org>  Mon, 24 Jan 2011 18:43:57 +0100

font-manager (0.5.7-1) unstable; urgency=low

  * New upstream release.
  * Switch to use dh_python2.
  * debian/control:
    - Drop build-dep on python-support.
    - Update build-dependency on debhelper.
    - Build-depend on libsqlite3-dev.
    - Replace XS-Python-Version field with X-Python-Version.
  * Add sign-tags = True in debian/gbp.conf.
  * Update watch file to use debian.net's redirector.

 -- Alessio Treglia <alessio@debian.org>  Wed, 29 Dec 2010 01:44:08 +0100

font-manager (0.5.6-1) unstable; urgency=low

  * New upstream release.
  * Build-Depends on libpango1.0-dev.
  * Add Vcs fields.
  * Drop 01-desktop_file.patch, applied upstream.
  * Pass -Wl,--as-needed to the linker.
  * Use 'dh  --with foo' for compatibility with DH v8.
  * Add the kind Andrea Colangelo to the Uploaders list.

 -- Alessio Treglia <alessio@debian.org>  Tue, 10 Aug 2010 15:59:35 +0200

font-manager (0.5.5-1) unstable; urgency=low

  * Initial release (Closes: #591790).

 -- Alessio Treglia <alessio@debian.org>  Thu, 05 Aug 2010 16:59:42 +0200
